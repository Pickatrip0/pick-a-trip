package com.example.sasin.googol;

import android.app.ProgressDialog;
import android.app.VoiceInteractor;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.sasin.googol.adapter.MainAdapter;
import com.example.sasin.googol.adapter.holder.CardViewHolder;
import com.example.sasin.googol.adapter.item.BaseItem;
import com.example.sasin.googol.adapter.item.CardViewItem;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MainAdapter adapter;
    static final int REQUEST_LOCATION = 1;
    private LocationManager locationManager;
    private Button req;
    private TextView t;
    double latti,longi;
    private RequestQueue requestQueue;
    private String locality,region;
    private DatabaseReference refCentral,refEast,refNorth,refSouth,refWest;
    private ArrayList<data_location> location_list;
    private List<String> head,deta;
    private String[] heads,details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_layout);
        req = findViewById(R.id.req);
        t = findViewById(R.id.t);
        requestQueue = Volley.newRequestQueue(this);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        refCentral = database.getReference().child("region").child("central");
        refEast = database.getReference().child("region").child("east");
        refNorth = database.getReference().child("region").child("north");
        refSouth = database.getReference().child("region").child("south");
        refWest = database.getReference().child("region").child("west");
        location_list = new  ArrayList<data_location>();
//        req.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                get_my_location();
//            }
//        });


        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        getLocation();


            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

            //adapter.setItemList(createItem());
//        adapter.setItemList(location_list);





    }

    public void get_location_list(){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("region").child(region).child(locality);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int posit = 0;
                head = new ArrayList<String>();
                deta = new ArrayList<String>();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    data_location data = postSnapshot.getValue(data_location.class);
                    Log.d("data_" , String.valueOf(data.head));
                    head.add(data.head);
                    deta.add(data.details);
                    location_list.add(data);
                }

                heads = new String[head.size()];
                heads = head.toArray(heads);

                details = new String[deta.size()];
                details = deta.toArray(details);

                adapter = new MainAdapter(location_list);

                recyclerView.setAdapter(adapter);
//
//                setcustomadapter();
                adapter.setOnItemClickListener(new MainAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        /*Integer pos = position;
                        Toast.makeText(HomeActivity.this,pos.toString(),Toast.LENGTH_LONG).show();*/
                        Intent listclick = new Intent(HomeActivity.this,DescriptionActivity.class);
                        listclick.putExtra("name",heads[position]);
                        listclick.putExtra("description",details[position]);
                        listclick.putExtra("path","region/" + region + "/" + locality + "/" + "0" + (position+1));

                        startActivity(listclick);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void get_my_location(){
        String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+latti+","+longi +"&sensor=true";
        Log.d("json",url);
        JsonObjectRequest request = new JsonObjectRequest(url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String address = response.getJSONArray("results").getJSONObject(0).getString("formatted_address");
//                                    Log.d("json",response.toString());
                            Log.d("json",response.getJSONArray("results").getJSONObject(6).getJSONArray("address_components").getJSONObject(0).getString("long_name"));
                            locality = response.getJSONArray("results").getJSONObject(6).getJSONArray("address_components").getJSONObject(0).getString("long_name");
                            String local = response.getJSONArray("results").getJSONObject(6).getJSONArray("address_components").getJSONObject(0).getString("long_name");
//                            t.setText(local);

                            refCentral.child(locality).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists()){
//                                                Toast.makeText(getApplicationContext(),"Have",Toast.LENGTH_LONG).show();
                                        region = "central";
                                        get_location_list();
                                    }//else  Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            refEast.child(locality).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists()){
//                                                Toast.makeText(getApplicationContext(),"Have",Toast.LENGTH_LONG).show();
                                        region = "east";
                                        get_location_list();
                                    }//else Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            refNorth.child(locality).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()){
//                                                Toast.makeText(getApplicationContext(),"Have",Toast.LENGTH_LONG).show();
                                        region = "north";
                                        get_location_list();
                                    }//else Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            refSouth.child(locality).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()){
//                                                Toast.makeText(getApplicationContext(),"Have",Toast.LENGTH_LONG).show();
                                        region = "south";
                                        get_location_list();
                                    }//else Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            refWest.child(locality).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists()){
//                                                Toast.makeText(getApplicationContext(),"Have",Toast.LENGTH_LONG).show();
                                        region = "west";
                                        get_location_list();
                                    }//else Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(request);


    }

//    private void collectdataandchatdata(Map<String,Object> users) {
//
//        ArrayList<String> comment = new ArrayList<>();
//        ArrayList<String> head = new ArrayList<>();
//
//        comment.add(comments);
//        head.add(heads);
//
//        //iterate through each user, ignoring their UID
//            for (Map.Entry<String, Object> entry : users.entrySet()){
//
//                //Get user map
//                Map singleUser = (Map) entry.getValue();
//                //Get phone field and append to list
//                comment.add((String)singleUser.get("messageDescription"));
//                head.add(" ");
//
//
//        }
//    }

    void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                latti = location.getLatitude();
                longi = location.getLongitude();
                get_my_location();
//                get_location_list();
            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_LOCATION:
                getLocation();
                break;
        }
    }


//    private List<BaseItem> createItem() {
//        List<BaseItem> itemList = new ArrayList<>();
//        itemList.add(new CardViewItem()
//                .setImage(R.drawable.chakrit)
//                .setText("Hello Chakrit"));
//                //.setRating(4.2f));
//        itemList.add(new CardViewItem()
//                .setImage(R.drawable.champ)
//                .setText("Hello Champ"));
//                //.setRating(4.8f));
//        itemList.add(new CardViewItem()
//                .setImage(R.drawable.wut)
//                .setText("Hello Wut"));
//                //.setRating(4.5f));
//        itemList.add(new CardViewItem()
//                .setImage(R.drawable.krittawat)
//                .setText("Hello Krittawat"));
//                //.setRating(4.4f));
//        itemList.add(new CardViewItem()
//                .setImage(R.drawable.peeranat)
//                .setText("Hello Peeranat"));
//                //.setRating(4.4f));
//        return itemList;
//    }
}


package com.example.sasin.googol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DetailZActivity extends AppCompatActivity {

    private DatabaseReference mDatabse;

    ImageView main_Pic;
    TextView Text_header,Text_data;
    RatingBar rate3star;
    ListView comment_fromUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_z);

        main_Pic = findViewById(R.id.MainPic);
        Text_header = findViewById(R.id.Header);
        Text_data = findViewById(R.id.Data);
        rate3star = findViewById(R.id.rateBar);
        comment_fromUser = findViewById(R.id.list_comment);

        mDatabse = FirebaseDatabase.getInstance().getReference();
    }
}

package com.example.sasin.googol.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.sasin.googol.HomeActivity;
import com.example.sasin.googol.adapter.holder.BaseViewHolder;
import com.example.sasin.googol.R;
import com.example.sasin.googol.adapter.holder.CardViewHolder;
import com.example.sasin.googol.adapter.item.BaseItem;
import com.example.sasin.googol.adapter.item.CardViewItem;
import com.example.sasin.googol.data_location;

import java.util.ArrayList;
import java.util.List;


public class MainAdapter extends RecyclerView.Adapter<CardViewHolder> {

    private ArrayList<data_location> itemList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;

//        if (viewType == com.example.sasin.googol.adapter.ViewType.TYPE_CARD_VIEW) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item, parent, false);
            return new CardViewHolder(v,mListener);
//        }

//        throw new RuntimeException("type is not match");
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        data_location data = itemList.get(position);
//        if (holder instanceof CardViewHolder) {
//            CardViewItem cardViewItem = (CardViewItem) i;
//            ((CardViewHolder) holder).setImage(cardViewItem.getImage());
//            ((CardViewHolder) holder).setText(cardViewItem.getText());
//        }

        holder.head.setText(data.head);

    }

    @Override
    public int getItemCount() {
        if (!itemList.isEmpty() || itemList != null) {
            return itemList.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
//     return itemList.get(position).getType();
    }


//
    public MainAdapter(ArrayList<data_location> data_locations){
        this.itemList = data_locations;
    }
}

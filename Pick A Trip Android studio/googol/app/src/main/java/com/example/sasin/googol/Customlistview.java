package com.example.sasin.googol;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Customlistview extends BaseAdapter {

    Context mContext;
    String[] sHead,sDescription;


    public Customlistview(Context context, String[] strHead, String[] strDescription){
        this.mContext = context;
        this.sHead = strHead;
        this.sDescription = strDescription;
    }
    @Override
    public int getCount() {
        return sDescription.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null){
            view = mInflater.inflate(R.layout.customlistview,parent,false);
        }

        TextView mHeader = (TextView) view.findViewById(R.id.cusHead1);
        mHeader.setText(sHead[position]);

        TextView mDescription = (TextView)view.findViewById(R.id.cusDescription1);
        mDescription.setText(sDescription[position]);

        return view;
    }

}

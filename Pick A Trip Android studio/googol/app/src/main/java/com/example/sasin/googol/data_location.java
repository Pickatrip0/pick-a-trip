package com.example.sasin.googol;

/**
 * Created by chakr on 3/7/2018.
 */

public class data_location {
    public String head;
    public String details;

    public data_location(){
        this.head = "";
        this.details = "";
    }

    public data_location(String head, String details) {
        this.head = head;
        this.details = details;
    }
}

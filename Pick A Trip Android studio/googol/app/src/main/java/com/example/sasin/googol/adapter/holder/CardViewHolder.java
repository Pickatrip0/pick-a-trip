package com.example.sasin.googol.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sasin.googol.HomeActivity;
import com.example.sasin.googol.R;
import com.example.sasin.googol.adapter.MainAdapter;
import com.example.sasin.googol.adapter.item.CardViewItem;


public class CardViewHolder extends BaseViewHolder {

    public ImageView imageView;
    public TextView head;

    public CardViewHolder(View itemView, final MainAdapter.OnItemClickListener listener) {
        super(itemView);

        imageView = itemView.findViewById(R.id.imageView);
        head =  itemView.findViewById(R.id.textView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION){
                        listener.onItemClick(position);
                    }
                }
            }
        });
    }

    public void setImage(int image) {
        imageView.setImageResource(image);
    }

    public void setText(String text) {
        head.setText(text);
    }
}

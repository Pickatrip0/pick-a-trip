package com.example.sasin.googol.adapter;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.sasin.googol.adapter.holder.CardViewHolder;
import com.example.sasin.googol.adapter.item.BaseItem;
import com.example.sasin.googol.adapter.item.CardViewItem;

import java.util.ArrayList;
import java.util.List;

import static android.content.ComponentName.readFromParcel;

/**
 * Created by Tsusume on 07/03/2018.
 */

public class arraylist_senddatatofragment implements Parcelable {

    private List<BaseItem> itemList = new ArrayList<>();

    protected arraylist_senddatatofragment(Parcel in) {
        super();
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
       //itemList = in.readArrayList();


    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<arraylist_senddatatofragment> CREATOR = new Parcelable.Creator<arraylist_senddatatofragment>() {
        @Override
        public arraylist_senddatatofragment createFromParcel(Parcel in) {
            return new arraylist_senddatatofragment(in);
        }

        @Override
        public arraylist_senddatatofragment[] newArray(int size) {
            return new arraylist_senddatatofragment[size];
        }
    };
}

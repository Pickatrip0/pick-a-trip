package com.example.sasin.googol;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.UUID;

public class CreateaccountActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText mFirstname,mLastname,mEmail,mPassword,mConfirmPassword;
    private Button mCreateaccount;
    private ImageButton imageButton;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private Uri filePath;
    private final int PICK_IMAGE_REQUEST = 71;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createaccount);

        mAuth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        mFirstname = (EditText)findViewById(R.id.firstname);
        mLastname = (EditText)findViewById(R.id.lastname);
        mEmail = (EditText)findViewById(R.id.registeremail);
        mPassword = (EditText)findViewById(R.id.registerpassword);
        mConfirmPassword = (EditText)findViewById(R.id.confirmregisterpassword);
        mCreateaccount = (Button)findViewById(R.id.createaccount);
        imageButton = (ImageButton)findViewById(R.id.create_image);


        mCreateaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateForm()){
                    return;
                }

                String pass = mPassword.getText().toString();
                String cpass = mConfirmPassword.getText().toString();
                if (pass.equals(cpass)){
                    String email = mEmail.getText().toString();
                    mConfirmPassword.setError(null);
                    registor(email,pass);
                    updateUI(0);
                }
                else {
                    mConfirmPassword.setError("Not match.");
                }
            }
        });

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(gallery,PICK_IMAGE_REQUEST);
            }
        });

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null ){

            filePath = data.getData();
            Log.d("img path","filePath = " + filePath);
            imageButton.setImageURI(filePath);
        }

    }
    @Override
    public void onStart() {
        super.onStart();
        updateUI(1);
    }

    private void registor(final String email, final String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(CreateaccountActivity.this,"createUserWithEmail:success",Toast.LENGTH_SHORT).show();
                            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    String path = "userId/Description/" + user.getUid() + "/picture";
                                    String name = mFirstname.getText().toString() + " " + mLastname.getText().toString();
                                    updateUserprofile(user,name);
                                    //senddatatodatabase(path,data);
                                    uploadImage(path);
                                    mAuth.signOut();
                                }
                            });
                        } else {
                            Toast.makeText(CreateaccountActivity.this,"createUserWithEmail:Error",Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

    private void updateUserprofile(FirebaseUser user, String name) {

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(CreateaccountActivity.this, "User updated", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private boolean validateForm() {
        boolean valid = true;

        String firstname = mFirstname.getText().toString();
        if (TextUtils.isEmpty(firstname)) {
            mFirstname.setError("Required.");
            valid = false;
        } else {
            mFirstname.setError(null);
        }

        String lastname = mLastname.getText().toString();
        if (TextUtils.isEmpty(lastname)) {
            mLastname.setError("Required.");
            valid = false;
        } else {
            mLastname.setError(null);
        }

        String email = mEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmail.setError("Required.");
            valid = false;
        } else {
            mEmail.setError(null);
        }

        String password = mPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPassword.setError("Required.");
            valid = false;
        } else {
            mPassword.setError(null);
        }

        String cpassword = mConfirmPassword.getText().toString();
        if (TextUtils.isEmpty(cpassword)) {
            mConfirmPassword.setError("Required.");
            valid = false;
        } else {
            mConfirmPassword.setError(null);
        }

        return valid;
    }

    public void senddatatodatabase(String path,String data){
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null){
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference(path);
            myRef.setValue(data);
            Toast.makeText(CreateaccountActivity.this,"Data Send!! Via:"+ user.getUid(),Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(CreateaccountActivity.this,"Please sign in!!",Toast.LENGTH_SHORT).show();
        }

    }

    private void updateUI(int a) {
        if (a == 1) {
            findViewById(R.id.create_account_field).setVisibility(View.VISIBLE);
            findViewById(R.id.create_account_successful_field).setVisibility(View.GONE);
        } else {
            findViewById(R.id.create_account_field).setVisibility(View.GONE);
            findViewById(R.id.create_account_successful_field).setVisibility(View.VISIBLE);
        }
    }

    private void uploadImage(final String refer) {

        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            StorageReference ref = storageReference.child(refer);
            ref.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                           /* Uri downloadUrl = taskSnapshot.getDownloadUrl();
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef = database.getReference(refer);
                            myRef.setValue(downloadUrl.toString());*/
                            progressDialog.dismiss();
                            Toast.makeText(CreateaccountActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(CreateaccountActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }
}

package com.example.sasin.googol;

import com.example.sasin.googol.adapter.item.BaseItem;
import com.example.sasin.googol.adapter.item.CardViewItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tsusume on 07/03/2018.
 */

public class setfragmentdata {

    public static List<BaseItem> createItem() {
        List<BaseItem> itemList = new ArrayList<>();
        itemList.add(new CardViewItem()
                .setImage(R.drawable.chakrit)
                .setText("Hello Chakrit"));
        //.setRating(4.2f));
        itemList.add(new CardViewItem()
                .setImage(R.drawable.champ)
                .setText("Hello Champ"));
        //.setRating(4.8f));
        itemList.add(new CardViewItem()
                .setImage(R.drawable.wut)
                .setText("Hello Wut"));
        //.setRating(4.5f));
        itemList.add(new CardViewItem()
                .setImage(R.drawable.krittawat)
                .setText("Hello Krittawat"));
        //.setRating(4.4f));
        itemList.add(new CardViewItem()
                .setImage(R.drawable.peeranat)
                .setText("Hello Peeranat"));
        //.setRating(4.4f));
        return itemList;
    }
}

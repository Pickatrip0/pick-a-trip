package com.example.sasin.googol.adapter.item;

import com.example.sasin.googol.adapter.ViewType;

import java.util.Objects;


public class CardViewItem extends BaseItem {

    private int cardViewImage;
    private String head;
   // private float rating;

    public CardViewItem() {
        super(ViewType.TYPE_CARD_VIEW);
    }

    public int getImage() {
        return cardViewImage;
    }

    public CardViewItem setImage(int cardViewImage) {
        this.cardViewImage = Objects.requireNonNull(cardViewImage);
        return this;
    }

    public String getText() {
        return head;
    }

    //public float getRating() {return rating;}

    public CardViewItem setText(String head) {
        this.head = head;
        return this;
    }

//    public CardViewItem setRating(float rating){
//        this.rating = rating;
//        return this;
//    }
}

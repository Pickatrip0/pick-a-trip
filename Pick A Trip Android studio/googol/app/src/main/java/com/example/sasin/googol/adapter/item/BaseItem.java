package com.example.sasin.googol.adapter.item;

public class BaseItem {

    private int type;

    public BaseItem(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}

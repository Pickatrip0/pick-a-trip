package com.example.sasin.googol;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotpasswordActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private EditText mEmail;
    private Button mForgotbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);

        mAuth = FirebaseAuth.getInstance();
        mEmail = (EditText)findViewById(R.id.forgotpasswordemail);
        mForgotbtn = (Button)findViewById(R.id.forgotpasswordbtn);

        mForgotbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validateForm()){
                    return;
                }
                String emailAddress = mEmail.getText().toString();
                mAuth.sendPasswordResetEmail(emailAddress)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ForgotpasswordActivity.this,"Send reset password link to email.",Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(ForgotpasswordActivity.this,"Error!!",Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        });

    }
    private boolean validateForm() {
        boolean valid = true;

        String email = mEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmail.setError("Required.");
            valid = false;
        } else {
            mEmail.setError(null);
        }

        return valid;
    }
}

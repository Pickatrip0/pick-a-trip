package com.example.sasin.googol;

import android.app.ListFragment;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

public class DescriptionActivity extends AppCompatActivity {

    private String names,dess,paths;
    private String[] namest,commentst;
    private EditText input;
    private ListView mComment;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            names = extras.getString("name");
            dess = extras.getString("description");
            paths = extras.getString("path");
        }
        Log.d("path_des",paths);

        mAuth = FirebaseAuth.getInstance();

        final ImageView mImage = (ImageView)findViewById(R.id.description_image);
        TextView mName = (TextView)findViewById(R.id.description_placename);
        TextView mDes = (TextView)findViewById(R.id.description_description);
        mComment = (ListView)findViewById(R.id.description_comment);

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference ref = storage.getReference(paths).child("01.jpeg");
        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.with(DescriptionActivity.this).load(uri).into(mImage);
                Log.d("uri", String.valueOf(uri));
            }
        });

        mName.setText(names);
        mDes.setText(dess);

        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
        Log.d("user alal", String.valueOf(currentUser));//currentUser
        DatabaseReference refe = FirebaseDatabase.getInstance().getReference(paths).child("comment");
        refe.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                collectdataandchatdata((Map<String,Object>) dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        FloatingActionButton fab =
                (FloatingActionButton)findViewById(R.id.fab);
        input = (EditText)findViewById(R.id.description_input);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validateForm()){
                    return;
                }
                FirebaseDatabase.getInstance()
                        .getReference(paths +"/comment")
                        .push()
                        .setValue(new sendcomment(input.getText().toString(),
                                FirebaseAuth.getInstance()
                                        .getCurrentUser()
                                        .getDisplayName())
                        );
                input.setText("");
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void collectdataandchatdata(Map<String,Object> users) {

        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> comment = new ArrayList<>();

        //iterate through each user, ignoring their UID
        if(users != null){
            for (Map.Entry<String, Object> entry : users.entrySet()){

                //Get user map
                Map singleUser = (Map) entry.getValue();
                //Get phone field and append to list
                name.add((String)singleUser.get("messageUserr"));
                comment.add((String)singleUser.get("messageComment"));
            }
        }

        namest = new String[name.size()];
        namest = name.toArray(namest);

        commentst = new String[comment.size()];
        commentst = comment.toArray(commentst);

        setcustomadapter();
    }


    private boolean validateForm() {
        boolean valid = true;

        String email = input.getText().toString();
        if (TextUtils.isEmpty(email)) {
            input.setError("Required.");
            valid = false;
        } else {
            input.setError(null);
        }

        return valid;
    }

    private void setcustomadapter(){

        mComment = (ListView)findViewById(R.id.description_comment);
        Customlistview adapter = new Customlistview(getApplicationContext(),namest,commentst);
        //adapter.filter();
        mComment.setAdapter(adapter);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            findViewById(R.id.description_commentbar).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.description_commentbar).setVisibility(View.GONE);
        }
    }
}

package com.example.tsusumesama.myapplication;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int[] resId = {R.drawable.ic_launcher_background,R.drawable.ic_launcher_background,R.drawable.ic_launcher_background
                ,R.drawable.ic_launcher_background,R.drawable.ic_launcher_background,R.drawable.ic_launcher_background
                ,R.drawable.ic_launcher_background,R.drawable.ic_launcher_background,R.drawable.ic_launcher_background};
        String[] values = new String[] {"1","2","3","4","5","6","7","8","9"};
        String[] des = new String[] {"11","22","33","44","55","66","77","88","99"};

       // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1, values);

        CustomAdapter adapter = new CustomAdapter(getApplicationContext(),values,des,resId);

        final ListView listView = (ListView)findViewById(R.id.myListview);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemValue = (String)listView.getItemAtPosition(position);

                Toast.makeText(MainActivity.this,"Position : "+position+" ListItem : "+itemValue, Toast.LENGTH_LONG).show();
            }
        });

       /* mylistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String itemValue = (String)mylistView.getItemAtPosition(position);

                Toast.makeText(MainActivity.this,"Position : "+position+" ListItem : "+itemValue, Toast.LENGTH_LONG).show();
            }
        });*/
    }

    public class CustomAdapter extends BaseAdapter{
        Context mContext;
        String[] strName,des;
        int[] resId;

        public CustomAdapter(Context context,String[] strName,String[] des,int[] resId){
            this.mContext = context;
            this.strName = strName;
            this.resId = resId;
            this.des = des;
        }

        public int getCount(){
            return strName.length;
        }

        public Object getItem(int position){
            return null;
        }

        public long getItemId(int position){
            return 0;
        }

        public View getView(int position, View view, ViewGroup parent){
            LayoutInflater mInflater =
                    (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(view == null)
                view = mInflater.inflate(R.layout.listviewlayout, parent, false);

            TextView textView = (TextView)view.findViewById(R.id.textView1);
            textView.setText(strName[position]);

            TextView description = (TextView)view.findViewById(R.id.description);
            description.setText(des[position]);

            ImageView imageView = (ImageView)view.findViewById(R.id.imageView1);
            imageView.setBackgroundResource(resId[position]);

            return view;
        }

    }


}
